defmodule TextClient do
  alias TextClient.Interact

  defdelegate start(),        to: Interact
  defdelegate start(server),  to: Interact
end
