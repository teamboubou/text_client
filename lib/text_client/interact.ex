defmodule TextClient.Interact do

  @hangman_server :"hangman@Ubuntu-Laptop"

  alias TextClient.{State, Player}

  def start(server \\ @hangman_server) do
    new_game(server)
    |> setup_state()
    |> Player.play()
  end

  defp setup_state(game) do
    %State{
      game_service: game,
      tally:        Hangman.tally(game),
    }
  end

  defp new_game(server) do
    Node.connect(server)
    :rpc.call(server,
              Hangman,
              :new_game,
              [])
  end

end
